
class Firewall:

    #constructor, which takes in a file
    def __init__(self, path_to_file):
        '''
        :param path_to_file: the filepath to load the csv file containing rules
        '''
        self.rules={}
        self.parse_rules(path_to_file)

    #function that determines if a packet should be accepted, given a direction (string), protocol (string), port (int), and an ip_address (string)
    def accept_packet(self, direction, protocol, port, ip_address):
        '''
        :param direction: inbound or oubound (string)
        :param protocol: tcp or udp (string)
        :param port: port number or range (int)
        :param ip_address: ip address or range (string)
        :return: boolean indicating if the packet should be accepted
        '''
        #check if port exists in rule tree, return false if it doesn't
        port_exists= direction in self.rules and protocol in self.rules[direction] and port in self.rules[direction][protocol]
        if(not port_exists):
            return False
        #check if exact ip address match
        ip_exists = ip_address in self.rules[direction][protocol][port]
        if(ip_exists):
            return True
        #go through ip address ranges and check if ip_address is in any of them, return true if there is a match
        for ips in self.rules[direction][protocol][port]:
            #only check ranges
            if '-' in ips:
                ip_range = ips.split('-')
                ip_start = [int(x) for x in ip_range[0].split('.')]
                ip_end = [int(x) for x in ip_range[1].split('.')]
                ip_to_check = [int(x) for x in ip_address.split('.')]
                if(self.ip_in_range(ip_to_check,ip_start,ip_end)):
                    return True
        return False

    #function thats builds a rule tree given a file containing csv rules
    def parse_rules(self,file):
        '''
        :param file: file containing csv rules
        :return: void, but builds the self.rules tree
        '''
        with open(file) as rules:
            for rule in rules:
                #get rid of newline char
                rule=rule.split('\n')[0]
                #split rules by comma into their individual segments
                rule_sbc= rule.split(",")
                bound=rule_sbc[0]
                protocol=rule_sbc[1]
                port=rule_sbc[2]
                ip=rule_sbc[3]
                #if a segment doesn't exist in the tree create it
                #inbound or outbound
                if bound not in self.rules:
                    self.rules[bound]={}
                #tcp or udp
                if protocol not in self.rules[bound]:
                    self.rules[bound][protocol]={}
                #port number
                if('-' in port):
                    port_range = port.split('-')
                    for iterated_port in range(int(port_range[0]),int(port_range[1])+1):
                        if iterated_port not in self.rules[bound][protocol]:
                            self.rules[bound][protocol][iterated_port] = {}
                        #ip address
                        self.rules[bound][protocol][iterated_port][ip]=1
                else:
                    if port not in self.rules[bound][protocol]:
                        self.rules[bound][protocol][int(port)] = {}
                    #ip address
                    self.rules[bound][protocol][int(port)][ip]=1

    #function that determines if one ip address is smaller than another
    def ip_is_smaller(self, ip1, ip2):#22.24.25.24 < 25.35.16.13
        '''
        determines if ip1 is smaller than ip2
        :param ip1: an ip address
        :param ip2: an ip address
        :return: true if ip1<ip2, false otherwise
        '''
        return ip1[0] < ip2[0] or (ip1[0] <= ip2[0] and ip1[1] < ip2[1]) or (ip1[0] <= ip2[0] and ip1[1] <= ip2[1] and ip1[2] < ip2[2]) or (ip1[0] <= ip2[0] and ip1[1] <= ip2[1] and ip1[2] <= ip2[2] and ip1[3] < ip2[3])

    #function that determines if an ip address is in a range of ip addresses (inclusive)
    def ip_in_range(self,ip,ip_start,ip_end):
        '''
        :param ip: the ip address to compare
        :param ip_start: ip range lower bound
        :param ip_end: ip range upper bound
        :return: true if ip is within the range of ip_start and ip_end, false otherwise
        '''
        return ip==ip_start or ip==ip_end or (self.ip_is_smaller(ip_start,ip) and self.ip_is_smaller(ip,ip_end))

#example of usage
# firewall = Firewall('rules.txt')
# print(firewall.accept_packet("outbound", "tcp", 10234, "192.168.10.11"))
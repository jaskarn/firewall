# Firewall

My approach was to parse the rules file and construct a psuedo decision tree, which would later be used by the accept_packet function to check if a packet should be accpeted. The tree is a python dictionary that contains nested dictionaries for direction, protocol, port, and ip_address. If there is a range of port numbers given in the rules file, a new item is created for every port in that range. If a range of ip address is given, it is inserted as is (ie if the rules file contains a range 128.0.0.1-256.0.0.1, the string "128.0.0.1-256.0.0.1" will be inserted in the nested dictionary) and is later checked to see if an ip address is in that range using helper functions. 

This method of using nested dictionaries allows for very fast checking of the direction, protocol, and port, so if a packet does not have any valid rules for those it will be rejected without having to check the ip addresses. 

Checking the ip addresses is slower, since there can be a range, and there are too many ip addresses to make a full decision tree for every port, so I thought a good tradeoff would be to check all the valid ranges of ip addresses assigned to a port, and see if the ip address given to accept_packet function is inside of any of the ranges.

I assumed all values in the csv rules file, the filepath for the constructor, and any input for the accept_packet function would be valid.

My testing was basically to run the example file and the example inputs given in the instructions, since I had no more time left, and my results matched those of the expected results in the instructions.

Since I only tested with a few rules in the test file, I do not know if a bigger file with >500000 lines would be too many rules for the in-memory python dictionary to handle, so that could be an optimization that could be done given more time.

---------


My ranking for teams would be:
1. Platform
2. Policy
3. Data

I am interested in the cluster management/infrastructure reporting/api support aspects of the platform team but I would like to know more about each team.